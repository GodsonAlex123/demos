var express = require('express');
var router = express.Router();
var app=express();
var loginRouter=require('./loginRouter')
var mongoUtil = require('../mongodbConnection');
var login=require('../jwtAuth');


// Add User 
router.post('/adduser',login.getToken, function(req, res) {
  var db = mongoUtil.getDb();
  res.setHeader('Content-Type', 'application/json');
  if(!req.body.userId || res.body == 'undefined' || res.body == {}){
    res.status(400).json({message:'Pleasr include some data and userId Parameter',status:'Error'});
    res.end();
  }
  db.collection("users").find({}).toArray(function(err, result)  {
    if(result.length!=0){
       result.forEach(element => {
          if(element.userId && element.userId == req.body.userId){
            res.json({message:'User Already Exist',status:'Success'})
          }
       });
    }
  });
  login.verifyJWTToken(req.token).then((token)=>{
    db.collection("users").insertOne(req.body, function(err, response) {
      if (err) throw err;
      res.json({message: "1 Record Inserted Successfully",status:'success',data:req.body});
    });
  }).catch((err)=>{
    res.status(400)
    .json({message: "Some error Occured",error:err,status:'error'})
  })
})

//Update user
router.post('/updateuser',login.getToken, function(req, res) {
  var db = mongoUtil.getDb();
  if(!req.query){
    res.status(400).json({message:'give params for update',status:'Error'})
  }
   if(res.body == 'undefined' || res.body == {}){
    res.status(400).json({message:'Insert some data',status:'Error'})
  }
  login.verifyJWTToken(req.token).then(()=>{
   var newvalues = { $set: req.body };
   
   var querry=req.query;
   db.collection("users").updateOne(querry, newvalues, function(err, resonse) {
    if (err) throw err;
    res.json({message: resonse,status:'success',data:req.body});
  });

  }).catch((err)=>{
    res.status(400)
    .json({message: "Some error Occured",err:err})
  })
});
//VIew All user
router.post('/viewuser', login.getToken,function(req, response) {
  var db = mongoUtil.getDb();
  console.log(req.body,"bodys")
  if(req.body == 'undefined'){
    req.body={};
  }
  if(isJsonString(req.body)){
    res.json({status:'error',message:''})
  }
  login.verifyJWTToken(req.token).then(()=>{
    db.collection("users").find(req.body).toArray(function(err, result)  {
      if (err) response.json({message:err,status:"error"});
      if(result.length==0){
        response.json({message:"NO data found",status:'success'})
      }
      response.send(result)
    });
   
  })
  
});
//delete user
router.get('/deleteuser',login.getToken, function(req, res) {
  console.log(loginRouter,loginRouter.currentUser,"bhh")
  var dbo = mongoUtil.getDb();
  if(!global.currentUser || ( global.currentUser && global.currentUser.role !='admin')){
    response.json({
      status:'error',
      message:'You Dont have permission to do this operation'
    })
  }
  if(req.body == 'undefined'){
    req.body={};
  }
   if(isJsonString(req.body)){
    res.json({status:'error',message:''})
  }
  dbo.collection("users").deleteMany(req.body, function(err, obj) {
    if (err) throw err;
    res.json({msg:obj.result.n + " document(s) deleted"});
  });
});

function isJsonString(str) {
  try {
      JSON.parse(str);
  } 
  catch (e) {
      return false;
  }
  return true;
}

module.exports = router;