const MongoClient = require( 'mongodb' ).MongoClient;
const url = "mongodb://localhost:27017";

var dbObj;
//mongo connection
module.exports = {

  connectToServer: function( callback ) {
    MongoClient.connect( url,  { useNewUrlParser: true }, function( err, db ) {
        dbObj  = db.db('demo');
      return callback( err ,dbObj);
    } );
  },

  getDb: function() {
    return dbObj;
  }
};